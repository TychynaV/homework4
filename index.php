<?php

ini_set('display_errors', 'on');

require_once __DIR__ . '/vendor/autoload.php';

use Task36\User;
use Task37\Date;
use Task38\ErrorHandler;

/**
 * Задача 36.1: Сделайте класс User, в котором будут следующие приватные свойства - name (имя), surname (фамилия), patronymic (отчество).
 * Задача 36.2: Сделайте так, чтобы при выводе объекта через echo на экран выводилось ФИО пользователя (фамилия, имя, отчество через пробел).
 */
$user = new User();
$user->name = 'Вася';
$user->surname = 'Пупкин';
$user->patronumic = 'Петрович';
echo $user;
echo '<br>****************************<br>';

/**
 * Задача 37.1: Сделайте класс Date с публичными свойствами year (год), month (месяц) и day (день).
 * Задача 37.2: С помощью магии сделайте так, чтобы в классе могли появляться новые свойства, без их прописывания в структуре класса, засетьте 2 таких свойства и выведите их на экран..
 * Задача 37.3: С помощью магии сделайте свойство weekDay, которое будет возвращать день недели, соответствующий дате.
 * Задача 37.4: Сделайте возможность вызова 2 методов, которые добавятся в наш класс со временем, но сейчас их нет или они не доступны.
 */

$date = new Date();
$date->year = 2019;
$date->mouth = 6;
$date->day = 13;

$date->hour = date('H'); // добавление нового свойства
$date->minute = date('i'); // добавление нового свойства
echo $date->hour . " - Сейчас часов";
echo $date->minute . " - Сейчас минут";

echo $date->weekDay; //37.3
echo '<br>****************************<br>';
