<?php

namespace Task36;

class User
{

    /** @var string  */
    public string $name;

    /** @var string  */
    public string $surname;

    /** @var string  */
    public string $patronymic;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "$this->surname" . " " . "$this->name" . " " . "$this->patronymic";
    }

}
