<?php

namespace Task37;

class Date
{

    private $year;

    private $mouth;

    private $day;

    /**
     * @param string $name
     * @param string $value
     * @return string
     */
    public function __set(string $name, string $value)
    {
        return $this->$name = $value;
    }

    /**
     * @param $name
     * @return int
     */
    public function __get($name)
    {
        if ($name)
        switch ($name) {
            case 'weekDay':
                return date('w', mktime(0,0,0,$this->mouth,$this->day,$this->year));
                break;
            default:
                return date('w', date('mdY'));
                break;
        }
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        $this->$name->$this($arguments);
    }
}
